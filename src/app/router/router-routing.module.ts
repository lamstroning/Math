import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {GraphComponent} from "../components/graph/graph.component";
import {AppComponent} from "../app.component";

const routes: Routes = [
  { path: '', component: GraphComponent },
  { path: '', component: AppComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RouterRoutingModule { }
