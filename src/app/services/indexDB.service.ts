import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class IndexedDbService {

  private db: IDBDatabase;

  constructor() { }

  public openDb(dbName: string, dbVersion: number, objectStores: string[]): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      const request = indexedDB.open(dbName, dbVersion);

      request.onerror = (event: any) => {
        console.error("Error opening indexedDB", event.target.errorCode);
        reject();
      };

      request.onsuccess = (event: any) => {
        console.log("IndexedDB connection opened successfully");
        this.db = event.target.result;
        resolve();
      };

      request.onupgradeneeded = (event: any) => {
        console.log("Upgrading IndexedDB version");
        const db = event.target.result;

        objectStores.forEach(store => {
          if (!db.objectStoreNames.contains(store)) {
            db.createObjectStore(store, { autoIncrement: true });
          }
        });
      };
    });
  }

  public addRecord(storeName: string, record: any, key = ''): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      const transaction = this.db.transaction([storeName], "readwrite");
      const objectStore = transaction.objectStore(storeName);
      const request = objectStore.add(record, key);

      request.onsuccess = (event: any) => {
        console.log(`Record added to ${storeName} object store`);
        resolve(event.target.result);
      };

      request.onerror = (event: any) => {
        console.error(`Error adding record to ${storeName} object store`);
        reject(event.target.errorCode);
      };
    });
  }

  public getAllRecords(storeName: string): Subject<any[]> {
    const resultSubject = new Subject<any[]>();

    const transaction = this.db.transaction([storeName], "readonly");
    const objectStore = transaction.objectStore(storeName);
    const request = objectStore.getAll();

    request.onsuccess = (event: any) => {
      console.log(`Retrieved all records from ${storeName} object store`);
      resultSubject.next(event.target.result);
      resultSubject.complete();
    };

    request.onerror = (event: any) => {
      console.error(`Error retrieving records from ${storeName} object store`);
      resultSubject.error(event.target.errorCode);
    };

    return resultSubject;
  }

  public deleteRecord(storeName: string, key: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      const transaction = this.db.transaction([storeName], "readwrite");
      const objectStore = transaction.objectStore(storeName);
      const request = objectStore.delete(key);

      request.onsuccess = (event: any) => {
        console.log(`Record with key ${key} deleted from ${storeName} object store`);
        resolve();
      };

      request.onerror = (event: any) => {
        console.error(`Error deleting record with key ${key} from ${storeName} object store`);
        reject(event.target.errorCode);
      };
    });
  }
}
