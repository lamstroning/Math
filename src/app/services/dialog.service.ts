import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import {DialogComponent} from "../components/UI-kit/dialog/dialog.component";
import {DialogData} from "../components/UI-kit/ui-kit.model";

const DIALOG_WIDTH = '80vw'
const DIALOG_HEIGHT = '80vh'

@Injectable()
export class DialogService {
  constructor(private dialog: MatDialog) {}

  openDialog(data: DialogData): Observable<any> {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: DIALOG_WIDTH,
      height: DIALOG_HEIGHT,
      data,
    });
    return dialogRef.afterClosed();
  }
}

