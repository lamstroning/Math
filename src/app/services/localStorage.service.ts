import {Injectable} from "@angular/core";
import {cloneDeepWith} from "lodash";

@Injectable({
  providedIn: 'root'
})

export class LocalStorageService {
  private storage: Storage;

  constructor() {
    this.storage = window.localStorage
  }

  public setItem(key: string, value: any): void {
    const serializedArray = JSON.stringify(value);

    this.storage.setItem(key, serializedArray)
  }

  public getItem<T>(key: string): T | null {
    const serializedValue = this.storage.getItem(key)
    if (serializedValue !== null) {
      const parsedValue = JSON.parse(serializedValue)
      return parsedValue as T
    }
    return null
  }

  public removeItem(key: string): void {
    this.storage.removeItem(key);
  }

  public clear(): void {
    this.storage.clear();
  }
}
