import {Injectable} from "@angular/core";
import {ReplaySubject} from "rxjs";
import {ThemePalette} from "@angular/material/core";
import {NotificationComponent} from "../components/UI-kit/notification/notification.component";
import {MatSnackBar} from "@angular/material/snack-bar";



export interface Notification {
  title: string
  body: string
  type: ThemePalette
}

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private dataSubject = new ReplaySubject<Notification | undefined>(3);
  data$ = this.dataSubject.asObservable();

  constructor(private _snackBar: MatSnackBar) {
  }

  openSnackBar(message = '', title = '') {
    this._snackBar.openFromComponent(NotificationComponent, {
      duration: 1000,
      verticalPosition: 'top',
      horizontalPosition: 'right',
      data: {
        message,
        title,
      }
    })
  }

  newNotification = (body = '', title = '' ,type: ThemePalette = 'primary', time = 500) => {
    const notification: Notification = {body, title, type}
    this.dataSubject.next(notification)

    if (time)
      setTimeout(() => this.dataSubject.next(undefined), time)
  }

  closeNotification = () => {
    this.dataSubject.next(undefined)
  }
}
