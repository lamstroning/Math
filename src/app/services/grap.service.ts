import { Injectable } from '@angular/core';
import {Edge, GraphNode, GraphNodesWithId, MAX_X, MAX_Y, MIN_X, MIN_Y, Point} from '../components/graph/graph.model';
import {Store} from "@ngrx/store";
import {setGraph, State, TCanvas} from "../../store/app.state";
import {cloneDeep} from "lodash";

@Injectable({
  providedIn: 'root'
})
export class GraphService {
  canvas: TCanvas | undefined
  radius: number
  nodesCount: number
  edgesCount: number

  constructor(private store: Store<State>) {
    this.store.select(state => state).subscribe(res => {
      this.canvas = res.canvasReducer.canvas
      this.nodesCount = res.graphReducer.nodesCount
      this.edgesCount = res.graphReducer.edgesCount
      this.radius = res.graphReducer.radius
    })
  }

  generateRandomGraph = (numNodes: number, numEdges: number): GraphNode[] => {
    if (!numNodes)
      return []

    const nodes: GraphNode[] = [...this.addNewNodes([], numNodes, false)]
    const possibleEdges: Edge[] = []


    nodes.map(node =>
      nodes.map(nextNode => possibleEdges.push({startNodeId: node.id, endNodeId: nextNode.id}))
    )


    for (let i = possibleEdges.length - 1; i >= 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [possibleEdges[i], possibleEdges[j]] = [possibleEdges[j], possibleEdges[i]];
    }

    for (let i = 0; i < numEdges; i++) {
      if (i >= possibleEdges.length)
        break

      const first = nodes.find((node) => node.id === possibleEdges[i].startNodeId)
      const second = nodes.find((node) => node.id === possibleEdges[i].endNodeId)
      this.addConnect(first!, second!)
    }

    localStorage.setItem('settings', JSON.stringify({nodesCount: this.nodesCount, edgesCount: this.edgesCount, radius: this.radius}))
    return nodes
  }

  createNode = ({id, position = this.getRandomCoordinates(), color = 'black', connections = []}: GraphNode
  ): GraphNode => ({
    id,
    position,
    connections,
    color,
  })

  getRandomCoordinates = (minX = MIN_X, maxX = this.canvas?.width || MAX_X, minY = MIN_Y, maxY = this.canvas?.height || MAX_Y): Point => {

    const x = Math.floor(Math.random() * (maxX - minX) + minX)
    const y =  Math.floor(Math.random() * (maxY - minY) + minY)

    return {x, y}
  }

  addConnect = (node: GraphNode, nextNode: GraphNode) => {
    node.connections.push(nextNode)
    nextNode.connections.push(node)
  }

  removeConnect = (node: GraphNode, nextNode: GraphNode) =>  {
    node.connections = node.connections.filter(({id}) => id !== nextNode.id)
    nextNode.connections = nextNode.connections.filter(({id}) => id !== node.id)
  }


  dispatchGraph = (nodes: GraphNode[], radius = this.radius, count = this.nodesCount, edges = this.edgesCount) => {
    if (nodes.length === 0)
      return this.store.dispatch(setGraph(nodes, radius, count, edges))
    if (!nodes.find(node => node.isStart)) {
      nodes[0].isStart = true
      delete nodes[0].isEnd
    }

    if (nodes.length > 1 && !nodes.find(node => node.isEnd))
      nodes.slice(-1)[0].isEnd = true

    this.store.dispatch(setGraph(nodes, radius, count, edges))
  }


  delete = () => {
    this.dispatchGraph([])
  }

  generate = () => {
    const nodeList = this.generateRandomGraph(this.nodesCount, this.edgesCount)
    this.dispatchGraph(nodeList)
  }

  addLinks = (nodeList: GraphNodesWithId[]): GraphNode[] => {
    const graphNodeList = nodeList.map((node: GraphNodesWithId) => ({...node, connections: []} as GraphNode))

    graphNodeList.map(graphNode => {
      const node = nodeList.find(item => item.id === graphNode.id) || {} as GraphNodesWithId
      const connectedNodes: GraphNode[] = []

      node.connections.map((id => {
        const connect = graphNodeList.find(item => id === item.id)
        connect && connectedNodes.push(connect)
      }))

      graphNode.connections = connectedNodes
    })

    return graphNodeList
  }

  removeLinks = (nodeList: GraphNode[]): GraphNodesWithId[] =>
      cloneDeep(nodeList).reduce((newList: GraphNodesWithId[], node: GraphNode) => {
        const connections = node.connections.map((node) => node.id)
        return [...newList, {...node, connections}]
      }, [])

  removeNode = (nodes: GraphNode[], node?: GraphNode) => {
    if (!node)
      return
    const newNodeList = cloneDeep(nodes).filter(item => {
      item.connections = item.connections.filter(connectedItem => connectedItem.id !== node.id)

      return item.id !== node.id
    })

    this.dispatchGraph(newNodeList)
  }

  newNodeId = (nodeList: GraphNode[]) =>
    Math.max(...nodeList.map(({id}) => id), 0) + 1

  addNewNode = (nodeList: GraphNode[], position = this.getRandomCoordinates(), color = 'black') => {
    const id = this.newNodeId(nodeList)
    const isEnd = !!nodeList.find(node => node.isEnd) || undefined
    const isStart = !!nodeList.find(node => node.isStart) || undefined

    const newNodeList = [...nodeList, this.createNode({id, position, color, isEnd, isStart, connections: []})]

    this.dispatchGraph(newNodeList)
    return newNodeList
  }

  addNewNodes = (nodeList: GraphNode[], count = 1, dispatch = true) => {
    let id = this.newNodeId(nodeList)
    const newNodes = []
    let i = 0

    while (i++ < count) {
      const newNode = this.createNode({id, position: this.getRandomCoordinates(), color: 'black', connections: []})
      newNodes.push(newNode)
      id++
    }

    if (!!nodeList.find(node => node.isStart) || undefined)
      newNodes[0].isStart = true
    if (nodeList.length > 1 && !!nodeList.find(node => node.isEnd) || undefined)
      newNodes.at(-1)!.isEnd = true

    const newNodeList = [...nodeList, ...newNodes]

    if (dispatch)
      this.dispatchGraph(newNodeList)
    return newNodeList
  }
}
