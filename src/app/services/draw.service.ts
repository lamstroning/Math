import {GraphNode, Point} from "../components/graph/graph.model";
import {Store} from "@ngrx/store";
import {State, TCanvas, TContext} from "../../store/app.state";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class DrawService {
  ctx: TContext | undefined
  canvas: TCanvas | undefined
  nodes: GraphNode[] = []
  radius: number

  constructor(private store: Store<State>) {
  store.select(state => state)
    .subscribe(({canvasReducer, graphReducer}) => {
      if (canvasReducer && graphReducer) {
        this.ctx = canvasReducer.context
        this.canvas = canvasReducer.canvas
        this.nodes = graphReducer.nodes
        this.radius = graphReducer.radius
      }})
  }

  drawNode = (node: GraphNode) => {
    if (this.ctx) {
      const text = `${node.isStart && 'start' || node.isEnd && 'end' || node.id}`
      this.ctx.beginPath()
      this.ctx.strokeStyle = node.active ? 'red' : node.color
      this.ctx.arc(node.position.x, node.position.y, this.radius, 0, 2 * Math.PI)
      this.ctx.fill()
      this.ctx.stroke()
      this.ctx.strokeText(text, node.position.x - this.radius / 2, node.position.y)
    }
  }

  redrawGraph = () => {
    this.clear()
    this.drawGraph(this.nodes)
  }

  clear = () => {
    if (this.ctx && this.canvas) {
      this.ctx.beginPath()
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
    }
  }


  drawEdge = (node: GraphNode, cords: Point, color = node?.color || 'black') => {
    if (this.ctx && cords && node) {
      this.ctx!.beginPath()
      this.ctx!.moveTo(node.position.x, node.position.y)
      this.ctx!.lineTo(cords.x, cords.y)
      this.ctx!.strokeStyle = color
      this.ctx!.stroke()
    }
  }

  drawConnections = (node: GraphNode, color = node.color) =>
    this.ctx && node.connections.map(sibling => this.drawEdge(node, sibling.position, color))

  drawGraph = (nodes: GraphNode[]) => {
    const {ctx, canvas, radius} = this

    if (!ctx || !canvas || !nodes)
      return

    // Draw edges
    ctx.lineWidth = 2

    nodes.map((node) => this.drawConnections(node))

    // Draw nodes
    ctx.fillStyle = 'white'
    ctx.lineWidth = 2
    ctx.font = `${radius}px serif`

    nodes.map(this.drawNode)
  }
}
