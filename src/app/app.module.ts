import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import {StoreModule} from '@ngrx/store'
import {AppComponent} from './app.component'
import {reducers} from '../store/app.state';
import {RouterRoutingModule} from "./router/router-routing.module";
import {FormsModule} from "@angular/forms";
import {GraphModule} from "./components/graph/graph.module";
import {UiKitModule} from "./components/UI-kit/uikit.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    RouterRoutingModule,
    StoreModule.forRoot(reducers),
    FormsModule,
    GraphModule,
    UiKitModule,
    BrowserAnimationsModule,
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
