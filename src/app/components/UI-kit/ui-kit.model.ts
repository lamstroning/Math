import {ThemePalette} from "@angular/material/core";
import {TemplateRef} from "@angular/core";


export interface DialogAction {
  text?: string,
  color: ThemePalette,
  close?: boolean,
  onClick?: (event: any) => void,
  disabled?: boolean
}
export interface DialogData {
  title?: string;
  content?: TemplateRef<any>;
  confirmText?: string;
  cancelText?: string;
  actions?: DialogAction[];
}

export const DEFAULT_FOOTER: DialogAction[] = [
  {
    text: 'Close',
    color: 'warn',
  },
  {
    text: 'confirm',
    color: 'primary',
    onClick: () => console.log('confirmed')
  }
]
