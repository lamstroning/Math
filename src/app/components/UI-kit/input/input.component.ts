import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
})
export class InputComponent implements OnInit {
  @Input() placeholder: string = ''
  @Input() value: string | number = ''
  @Input() type: string = 'text'
  @Input() name: string = ''
  @Input() label: string = ''
  @Input() min: number
  @Input() max: number
  @Input() prefix: any
  @Output() onChange: EventEmitter<string | number> = new EventEmitter<string | number>()

  inputValue: string = ''

  constructor() {}

  ngOnInit() {
    this.inputValue = this.value?.toString() || ''
  }

  checkLimit() {
    if (this.type === 'number') {
      let numberValue = parseInt(this.inputValue)
      if (!numberValue)
        numberValue = 0

      if (this.max)
        numberValue = Math.min(numberValue, this.max)
      if (this.min || this.min === 0)
        numberValue = Math.max(numberValue, this.min)

      this.inputValue = numberValue.toString()
    }
  }

  onInputChange() {
    this.onChange.emit(this.inputValue)
  }
}

