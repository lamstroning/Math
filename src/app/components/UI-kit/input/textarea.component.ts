import {Attribute, Component, EventEmitter, Input, OnInit, Output} from "@angular/core";

@Component({
  templateUrl: 'textarea.component.html',
  selector: 'app-textarea'
})
export class TextareaComponent implements OnInit  {
  @Input() placeholder: string = '';
  @Input() value: string | number = '';
  @Input() name: string = '';
  @Input() label: string = '';
  @Input() textareaClass: string = 'textarea'
  @Output() onChange: EventEmitter<string> = new EventEmitter<string>();
  inputValue: string = '';

  constructor() {

  }
  ngOnInit() {
    this.inputValue = this.value?.toString() || ''
  }

  onInputChange() {
    this.onChange.emit(this.inputValue);
  }
}
