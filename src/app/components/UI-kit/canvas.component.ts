import {Component, ViewChild, AfterViewInit, ElementRef, HostListener} from '@angular/core'
import {Store} from "@ngrx/store";
import {setCanvas, CanvasState} from "../../../store/app.state";

@Component({
  selector: 'app-canvas',
  template: '<canvas #myCanvas></canvas>'
})
export class CanvasComponent implements AfterViewInit {
  @ViewChild('myCanvas') canvas!: ElementRef<HTMLCanvasElement>

  constructor(private store: Store<CanvasState>) {}

  @HostListener('window:resize', ['$event'])
  onResize() {
    const canvas = this.canvas?.nativeElement
    if (!canvas)
      return
    const{offsetHeight, offsetWidth} = canvas.parentElement || {}
    canvas.width = offsetWidth || 0
    canvas.height = offsetHeight || 0
  }

  ngAfterViewInit(): void {
    this.onResize()
    const canvas = this.canvas.nativeElement

    if (canvas) {
      const context = canvas.getContext('2d')
      this.store.dispatch(setCanvas(canvas, context))
    }
  }
}
