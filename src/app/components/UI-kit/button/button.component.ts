import { Component, Input, Output, EventEmitter } from '@angular/core';
import {ThemePalette} from "@angular/material/core";

enum MatButton {
  button = 'mat-button',
  icon = 'mat-icon-button',
  fab = 'mat-fab',
  raised = 'mat-raised-button',
}

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
})
export class ButtonComponent {
  @Input() label: string;
  @Input() color?: ThemePalette;
  @Input() disabled: boolean = false;
  @Input() icon: string = ''
  @Input() link: boolean = false
  @Input() buttonType: MatButton
  @Output() clicked: EventEmitter<void> = new EventEmitter<void>();

  constructor() {
    if (!this.buttonType) {
      if (this.icon && !this.label)
        this.buttonType = this.color ? MatButton.fab : MatButton.icon
      else if (!this.icon && this.label)
        this.buttonType = this.color ? MatButton.raised : MatButton.button
    }
  }


  onClick(): void {
    if (!this.disabled) {
      this.clicked.emit();
    }
  }
}
