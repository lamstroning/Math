import {Component, EventEmitter, OnInit, Output} from "@angular/core";

@Component({
  selector: 'app-dialog-footer',
  templateUrl: 'dialog-footer.component.html'
})
export class DialogFooterComponent implements  OnInit{
  @Output() closeDialog: EventEmitter<void> = new EventEmitter<void>();
  @Output() onConfirm: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  ngOnInit() {
  }


  emmitCloseDialog() {
    this.closeDialog.emit()
  }

  emmitConfirm() {
    this.onConfirm.emit()
  }
}
