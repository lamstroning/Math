import {Component, Inject, OnInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DEFAULT_FOOTER, DialogAction, DialogData} from "../ui-kit.model";

@Component({
  selector: 'app-dialog',
  templateUrl: 'dialog.component.html',
})
export class DialogComponent implements OnInit {
  footer: DialogAction[]
  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  ngOnInit() {
    this.footer = this.data.actions ? this.data.actions : DEFAULT_FOOTER
  }

  onCancel = (): void => {
    this.dialogRef.close(true);
  }
}
