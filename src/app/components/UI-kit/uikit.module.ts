import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {InputComponent} from "./input/input.component";
import {DialogComponent} from "./dialog/dialog.component";
import {ButtonComponent} from "./button/button.component";
import {CanvasComponent} from "./canvas.component";
import {DialogFooterComponent} from "./dialog/dialog-footer/dialog-footer.component";
import {DialogHeaderComponent} from "./dialog/dialog-header/dialog-header.component";
import {NotificationComponent} from './notification/notification.component';
import {TextareaComponent} from "./input/textarea.component";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {DialogService} from "../../services/dialog.service";
import {
  MAT_SNACK_BAR_DATA,
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MatSnackBar,
  MatSnackBarModule
} from "@angular/material/snack-bar";
import {NotificationService} from "../../services/notification.service";

@NgModule({
  declarations: [
    ButtonComponent,
    InputComponent,
    TextareaComponent,
    DialogComponent,
    DialogFooterComponent,
    DialogHeaderComponent,
    CanvasComponent,
    NotificationComponent
  ],
  exports: [
    ButtonComponent,
    InputComponent,
    DialogComponent,
    CanvasComponent,
    DialogFooterComponent,
    NotificationComponent,
    TextareaComponent,
    MatButtonModule,
  ],
  providers: [
    DialogService,
    { provide: MAT_SNACK_BAR_DATA, useValue: {} }
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatInputModule,
    MatSnackBarModule
  ],
})
export class UiKitModule {}
