import {Component} from "@angular/core";
import {GraphNode, SavedNodes} from "../graph.model";
import {Store} from "@ngrx/store";
import {State} from "../../../../store/app.state";
import {cloneDeep} from "lodash";
import {GraphService} from "../../../services/grap.service";
import {DrawService} from "../../../services/draw.service";

@Component({
  selector: 'app-graph-node-list',
  templateUrl: './graph-node-list.html'
})
export class GraphNodeList {
  nodes: GraphNode[]

  constructor(
    private store: Store<State>,
    private graphService: GraphService,
    private drawService: DrawService
  ) {
    this.store.select(state => state.graphReducer).subscribe((graph) => {
      this.nodes = graph.nodes
    })
  }


changeAxis = (node: GraphNode, value: string | number, axis: 'x' | 'y') => {
  const newNodeList = cloneDeep(this.nodes)
  const newNode = newNodeList.find(newNode => newNode.id === node.id)
  newNode!.position[axis] = +value
  this.graphService.dispatchGraph(newNodeList)
  this.drawService.redrawGraph()
}

trackBy = (index: number, node: GraphNode) => {
  return node.id
}


deleteNode = (node: GraphNode) => {
  this.graphService.removeNode(this.nodes, node)
  this.drawService.redrawGraph()
}
}
