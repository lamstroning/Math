import {AfterViewInit, Component, OnInit} from '@angular/core';
import {setGraph, State, TCanvas, TContext} from "../../../store/app.state";
import {Store} from "@ngrx/store";
import {Point, GraphNode, Edge, MAX_Y, MAX_X, MIN_X, MIN_Y, NODE_RADIUS, GraphNodesWithId} from "./graph.model";
import {DrawService} from "../../services/draw.service";
import { cloneDeep } from 'lodash';
import {LocalStorageService} from "../../services/localStorage.service";
import {GraphService} from "../../services/grap.service";

@Component({
  selector: 'app-graph',
  host: {
    'class': 'graph'
  },
  templateUrl: './graph.component.html',
})
export class GraphComponent implements OnInit, AfterViewInit {
  private ctx: TContext | undefined
  private canvas: TCanvas | undefined
  private nodes: GraphNode[] = []
  private selectedNode: GraphNode | undefined

  public nodesCount: number
  public edgesCount: number
  public color: number
  public radius: number = NODE_RADIUS

  constructor(
    private store: Store<State>,
    private drawService: DrawService,
    private localStorageService: LocalStorageService,
    private graphService: GraphService
  ) {
    this.drawService = new DrawService(store)
    this.localStorageService = new LocalStorageService()
  }

  ngOnInit() {
    this.store
      .select(state => {
        return state
      })
      .subscribe(({canvasReducer, graphReducer}) => {
        if (canvasReducer && graphReducer) {
          this.ctx = canvasReducer.context
          this.canvas = canvasReducer.canvas
          this.radius = graphReducer.radius
          this.nodesCount = graphReducer.nodesCount
          this.edgesCount = graphReducer.edgesCount

          this.nodes = graphReducer.nodes || this.graphService.generateRandomGraph(this.nodesCount, this.edgesCount)
        }
      })
  }

  ngAfterViewInit() {
    this.drawService.drawGraph(this.nodes)
  }


  getNodeFromCanvas = (coords: Point) => {
    const distance = (x1: number, y1: number, x2: number, y2: number) => Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);

    return this.nodes.find(({position: {x, y}}) => distance(coords.x, coords.y, x, y) <= NODE_RADIUS * 2)
  }

  mousemove = (event: MouseEvent) => {
    if (event.buttons == 1) {
      const coords: Point = {x: event.x, y: event.y}
      const nextNode = this.getNodeFromCanvas(coords)

      if (!this.selectedNode)
        this.selectedNode = this.nodes[this.nodes.length - 1]

      this.drawService.redrawGraph()

      nextNode
        ? this.drawService.drawEdge(this.selectedNode, nextNode.position)
        : this.drawService.drawEdge(this.selectedNode, coords)
    }
  }

  mousedown = (event: MouseEvent) => {
    const coords: Point = {x: event.x, y: event.y}

    this.selectedNode = this.getNodeFromCanvas(coords)

    this.selectedNode || this.graphService.addNewNode(this.nodes, coords)

    this.drawService.drawGraph(this.nodes)
  }

  mouseup = (event: MouseEvent) => {
    const coords: Point = {x: event.x, y: event.y}

    let nextNode = this.getNodeFromCanvas(coords)

    if (!nextNode) {
      this.graphService.addNewNode(this.nodes, coords)
      nextNode = this.nodes[this.nodes.length - 1]
      this.drawService.redrawGraph()
    }

    if (this.selectedNode && nextNode) {

      const newNodeList = cloneDeep(this.nodes)
      const currentNode = newNodeList.find(({id})=> id === this.selectedNode!.id)
      const newConnect = newNodeList!.find(({id}) => id === nextNode!.id)

      const connectedNode = currentNode!.connections.find(({id}) => id === newConnect!.id)


      connectedNode
        ? this.graphService.removeConnect(currentNode!, connectedNode)
        : this.graphService.addConnect(currentNode!, newConnect!)

      this.graphService.dispatchGraph(newNodeList)
    }
    this.drawService.redrawGraph()
  }

  dblclick = (event: MouseEvent) => {
    const coords: Point = {x: event.x, y: event.y}

    const node = this.getNodeFromCanvas(coords)
    if (this.selectedNode?.id === node?.id) {
      this.graphService.removeNode(this.nodes, node)
      this.drawService.redrawGraph()
    }
  }
}
