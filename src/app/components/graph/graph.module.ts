import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GraphComponent} from "./graph.component";
import {GraphControlComponent} from "./graphControlComponent/graph-control.component";
import {UiKitModule} from "../UI-kit/uikit.module";
import {NgxJsonViewerModule} from "ngx-json-viewer";
import { GraphSettingsComponent } from './graphSettings/graph-settings.component';
import {MatGridListModule} from "@angular/material/grid-list";
import {MatSidenavModule} from "@angular/material/sidenav";
import {GraphNodeList} from "./graphNodeList/graph-node-list";
import {MatSliderModule} from "@angular/material/slider";
import {FormsModule} from "@angular/forms";
import {MatCardModule} from "@angular/material/card";
import {MatTableModule} from "@angular/material/table";

@NgModule({
  declarations: [
    GraphComponent,
    GraphControlComponent,
    GraphSettingsComponent,
    GraphNodeList
  ],
  imports: [
    CommonModule,
    UiKitModule,
    NgxJsonViewerModule,
    MatGridListModule,
    MatSidenavModule,
    MatSliderModule,
    FormsModule,
    MatCardModule,
    MatTableModule
  ],
  exports: [
    GraphControlComponent,
    GraphComponent
  ],
})
export class GraphModule { }
