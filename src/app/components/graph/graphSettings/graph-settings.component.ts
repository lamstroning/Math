import {AfterViewChecked, AfterViewInit, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Store} from "@ngrx/store";
import {setSettings, State} from "../../../../store/app.state";
import {GraphNode, SavedNodes} from "../graph.model";
import {GraphService} from "../../../services/grap.service";
import {DrawService} from "../../../services/draw.service";
import {IndexedDbService} from "../../../services/indexDB.service";
import {NotificationService} from "../../../services/notification.service";

@Component({
  selector: 'app-graph-settings',
  templateUrl: './graph-settings.component.html',
})
export class GraphSettingsComponent implements OnInit{
  nodes: GraphNode[]
  savedList: SavedNodes[]
  displayedColumns = ["name", "count", "delete"]

  constructor(
    private store: Store<State>,
    private graphService: GraphService,
    private drawService: DrawService,
    private indexedDbService: IndexedDbService,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.store.select(state => state).subscribe(({graphReducer, settingsReducer}) => {
      this.nodes = graphReducer.nodes
      if (settingsReducer.savedList?.length)
        this.savedList = settingsReducer.savedList
      else
        this.getSavedList()
    })
  }

  getSavedList = () => this.getGraphRecords().subscribe(res => this.savedList = res)

  getGraphRecords = () => this.indexedDbService.getAllRecords('graph')

  saveName = (name: string) =>
    this.store.dispatch(setSettings(name, this.savedList))

  load = (savedGraph: SavedNodes) => {
    this.graphService.dispatchGraph(savedGraph.nodes)
    this.drawService.redrawGraph()
  }

  deleteRecord = (savedList: SavedNodes[], item: SavedNodes) => {
    this.indexedDbService.deleteRecord('graph', item.key).then(() => {
      // this.getSavedList()
    })
    this.notificationService.openSnackBar(`${item.key} - success removed!`, 'Success')
  }
}

