
import {
  EDGE_COUNT,
  NODE_COUNT,
  GraphNode,
  SavedNodes,
  NODES_LIMIT,
  EDGES_LIMIT,
  RADIUS_LIMIT,
} from "../graph.model";
import {cloneDeep} from "lodash";
import {
  Component, EventEmitter,
  HostListener,
  OnInit, Output,
  TemplateRef,
  ViewChild
} from "@angular/core";
import {Store} from "@ngrx/store";
import {setSettings, State} from "../../../../store/app.state";
import {DrawService} from "../../../services/draw.service";
import {GraphService} from "../../../services/grap.service";
import {IndexedDbService} from "../../../services/indexDB.service";
import {DialogService} from "../../../services/dialog.service";
import {bfs, copyToClipboard} from "../../../utils/utils";
import {NotificationService} from "../../../services/notification.service";
import {DialogData} from "../../UI-kit/ui-kit.model";
import {MatSnackBar} from "@angular/material/snack-bar";
import {NotificationComponent} from "../../UI-kit/notification/notification.component";


@Component({
selector: 'app-graph-control',
templateUrl: 'graph-control.component.html',

})
export class GraphControlComponent implements OnInit {
  @ViewChild('exportDialog') exportDialog: TemplateRef<any>;
  @ViewChild('importDialog') importDialog: TemplateRef<any>;
  @ViewChild('saveDialog') saveDialog: TemplateRef<any>;

  @Output('toggleMenu') toggleMenu = new EventEmitter()

  nodesCount = NODE_COUNT
  edgesCount = EDGE_COUNT
  radius = 10
  noWay = false

  nodes: GraphNode[] = []
  savedList: SavedNodes[] = []

  nodesLimit = NODES_LIMIT
  edgesLimit = EDGES_LIMIT
  radiusLimit = RADIUS_LIMIT
  saveName = ''

  jsonImport: string


  constructor(
    private store: Store<State>,
    private drawService: DrawService,
    private indexedDbService : IndexedDbService,
    private graphService: GraphService,
    private dialogService: DialogService,
    private notificationService: NotificationService,
  ) {}

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.drawService.redrawGraph()
  }


  ngOnInit() {
    this.store
      .select(state => state)
      .subscribe(({graphReducer, settingsReducer}) => {
        this.radius = graphReducer.radius
        this.nodesCount = graphReducer.nodesCount
        this.edgesCount = graphReducer.edgesCount
        this.nodes = graphReducer.nodes
        this.saveName = settingsReducer.saveName
      })

    this.initBD()
  }

  copy = () => {
    copyToClipboard(this.graphService.removeLinks(this.nodes))
    this.notificationService.openSnackBar('Copied', 'Success!')
  }

  close = (event: any = undefined) => event && event()

  save = () => {
    const newRecord: SavedNodes = {
      key: this.saveName,
      nodes: this.nodes,
    }

    const errors = `${this.nodes.length === 0 && 'Empty graph ' || ''}${!this.saveName && 'Empty name' || ''}`

    if (errors)
        return this.notificationService.openSnackBar(errors, 'Error')

    this.indexedDbService.addRecord('graph', newRecord, newRecord.key).then((key) => {
      this.notificationService.openSnackBar(`${newRecord.key} - success saved!`, 'success')
      this.dispatchSavedList()
    })
      .catch(err => {
        this.notificationService.openSnackBar('Error', 'Error')
      })

    this.drawService.redrawGraph()
  }

  dispatchSavedList = () => this.getGraphRecords().subscribe(res => this.store.dispatch(setSettings('', res)))

  getSavedList = () => this.getGraphRecords().subscribe(res => {
    this.savedList = res
  })

  getGraphRecords = () => this.indexedDbService.getAllRecords('graph')


  initBD = () => {
    this.indexedDbService.openDb('graph', 1, ['graph']).then(() => {
      this.getSavedList()
    })
  }

  openImportDialog = () => {
    const dialog: DialogData = {
      title: 'Import',
      content: this.importDialog,
      actions: [
        {
          text: 'cancel',
          color: 'accent',
          onClick: this.close,
        },
        {
          text: 'import',
          color: 'primary',
          onClick: this.import,
        },
      ],
    }

    this.dialogService.openDialog(dialog)
  }

  import = () => {
    try {
      const graphNodes = JSON.parse(this.jsonImport)

      const nodeList = this.graphService.addLinks(graphNodes)

      this.rerender(nodeList)
      this.notificationService.openSnackBar('Import graph', 'Success!')

    } catch {
      this.notificationService.openSnackBar('Parse error', 'Error')
    }

    this.close()
  }

  delete = () => {
    this.graphService.delete()
    this.drawService.redrawGraph()
  }

  openSettingsDialog  = () => {
    const dialog: DialogData = {
      title: 'Save',
      content: this.saveDialog,
      actions: [
        {
          text: 'cancel',
          color: 'accent',
          onClick: this.close,
        },
        {
          text: 'copy',
          color: 'primary',
          onClick: this.copy,
        },
        {
          text: 'save',
          color: 'primary',
          onClick: this.save,
        }
      ]
    }

    this.dialogService.openDialog(dialog)
  }

  toggleEditMenu = () => {
    this.toggleMenu.emit()
  }

  generate = () => {
    this.graphService.generate()
    this.drawService.redrawGraph()
  }

  getNodesWithoutLinks = () => {
    return this.graphService.removeLinks(this.nodes)
  }

  searchWay = () => {
    const nodeList = cloneDeep(this.nodes)
    const shortPath = bfs(nodeList)

    if (!shortPath) {
      this.noWay = true
      setTimeout(() => this.noWay = false, 300)
      return
    }

    shortPath.reduce((prevItem, item) => {
      if (!item.isStart)
        this.drawService.drawEdge(item, prevItem.position, 'red')
      return item
    }, shortPath[0])
  }

  rerender = (nodes = this.nodes, radius = this.radius, nodesCount = this.nodesCount, edgesCount = this.edgesCount) => {
    this.graphService.dispatchGraph(nodes, radius, nodesCount, edgesCount)
    this.rerender()
  }

  changeRadius = () => {
    const radius = Math.max(this.radius || Math.min(this.radius, RADIUS_LIMIT), 0)


    localStorage.setItem('settings', JSON.stringify({nodesCount: this.nodesCount, edgesCount: this.edgesCount, radius}))
    this.graphService.dispatchGraph(this.nodes, radius)
    this.drawService.redrawGraph()
  }

  changeNodes = (event: string | number) => {
    const count = Math.max(+event || Math.min(this.nodesCount, NODES_LIMIT), 0)
    this.graphService.dispatchGraph(
      this.nodes, this.radius, count, this.edgesCount
    )
  }

  changeEdges = (event: string | number) => {
    const edgesCount = Math.max(+event || Math.min(this.edgesCount, EDGES_LIMIT), 0)
    this.graphService.dispatchGraph(
      this.nodes, this.radius, this.nodesCount, edgesCount
    )
  }
}
