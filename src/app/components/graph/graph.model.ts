export const NODE_RADIUS = 16
export const NODE_COUNT = 10
export const EDGE_COUNT = 10
export const NODE_DIAMETER = NODE_RADIUS * 2

export const MAX_Y = window.innerHeight - NODE_DIAMETER
export const MAX_X = window.innerWidth - NODE_DIAMETER
export const MIN_X = NODE_DIAMETER
export const MIN_Y = NODE_DIAMETER

export const NODES_LIMIT = 5000
export const EDGES_LIMIT = 3000
export const RADIUS_LIMIT = 50

export interface Point {
  x: number;
  y: number;
}

export interface GraphNode {
  id: number;
  position: Point;
  connections: GraphNode[];
  color: string;
  opened?: boolean
  isEnd?: boolean;
  isStart?: boolean;
  active?: boolean;
  weight?: number
}

export interface GraphNodesWithId extends Omit<GraphNode, 'connections'>  {
  connections: number[]
}

export interface Edge {
  startNodeId: number;
  endNodeId: number;
}

export interface SavedNodes {
  key: string,
  nodes: GraphNode[]
}
