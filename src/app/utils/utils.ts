import {GraphNode} from "../components/graph/graph.model";

export const getRandomColor = () => {
  const letters = '0123456789ABCDEF'
  let color = '#'

  for (let i = 0; i < 6; i++)
    color += letters[Math.floor(Math.random() * 16)]
  return color;
}

export const copyToClipboard = (value: any) =>
{
  const tmp = document.createElement('textarea')
  tmp.value = JSON.stringify(value || '[]')
  document.body.appendChild(tmp)
  tmp.select()

  document.execCommand('copy');

  document.body.removeChild(tmp)
}


export const bfs = (nodeList: GraphNode[]): GraphNode[] | null => {
  const start = nodeList.find(node => node.isStart)

  if (!start)
    return null

  nodeList.forEach(node => node.opened = true)

  const queue: GraphNode[] = [start];
  const parent: {[key: string]: GraphNode | null} = { [start.id]: null };

  while (queue.length > 0) {
    const current = queue.shift()!

    if (!current.opened)
      continue;

    current.opened = false

    if (current.isEnd) {
      // Восстанавливаем путь от конечной точки до начальной
      const path: GraphNode[] = [];
      let node: GraphNode | null = current;
      while (node !== null) {
        path.push(node);
        node = parent[node.id]
      }
      return path.reverse()
    }

    current.connections.forEach(neighbor => {
      if (neighbor.opened) {
        queue.push(neighbor)
        parent[neighbor.id] = current
      }
    })
  }

  return null;
}
