import { createAction, createReducer, on } from '@ngrx/store';
import {GraphNode, NODE_COUNT, NODE_RADIUS, SavedNodes} from "../app/components/graph/graph.model";

export type TContext = CanvasRenderingContext2D | null
export type TCanvas = HTMLCanvasElement | null

export interface State {
  canvasReducer: CanvasState
  graphReducer: GraphState
  settingsReducer: SettingsState
}

export interface CanvasState {
  canvas?: TCanvas
  context?: TContext
}

export interface SettingsState {
  saveName: string,
  savedList: SavedNodes[]
}

export interface GraphState {
  nodes: GraphNode[]
  radius: number
  nodesCount: number
  edgesCount: number
}

const initGraphState = (): GraphState => {
  const settings = JSON.parse(localStorage.getItem('settings') || '{}')

  return ({
    nodes: [],
    nodesCount: settings.nodesCount || NODE_COUNT,
    edgesCount: settings.edgesCount || NODE_COUNT,
    radius: settings.radius || NODE_RADIUS
  })
}


export const setCanvas = createAction('[Canvas] Set Canvas', (canvas, context): CanvasState => ({canvas, context}))
export const setSettings = createAction('[Settings] Set Settings', (saveName, savedList): SettingsState => ({saveName, savedList}))
export const setGraph = createAction('[Graph] Set Graph', (nodes, radius, nodesCount, edgesCount): GraphState => ({nodes, nodesCount, edgesCount,radius}))

export const canvasReducer = createReducer(
  {},
  on(setCanvas, (state, {context, canvas}) => ({ ...state, context, canvas })),
)

export const graphReducer = createReducer(
  initGraphState(),
  on(setGraph, (state, {nodes, radius, nodesCount, edgesCount}) => ({...state, nodes, radius, nodesCount, edgesCount})),
)


const settingsReducerInit: SettingsState = {
  saveName: '',
  savedList: []
}

export const settingsReducer = createReducer(
  {
    settingsReducerInit
  },
  on(setSettings, (state, {saveName, savedList}) => ({...state, saveName, savedList})),
)

export const reducers = {
  canvasReducer,
  graphReducer,
  settingsReducer
}
